package List;

import java.util.*;

public class ListPractice {

    public static void main(String[] args) {
        // Part 1 to part 4
        /*
         * List<String> words = new ArrayList<String>();
         * System.out.println(words.size());
         * 
         * words.add("Aaaa");
         * words.add("b");
         * words.add("CCCC");
         * words.add("D");
         * 
         * for (String s : words) {
         * System.out.println(s);
         * }
         * 
         * System.out.println(words.size());
         * System.out.println(countUpperCase(words));
         * List<String> newArray=getUpperCase(words);
         * for(String s:newArray){
         * System.out.println(s);
         * }
         * 
         * System.out.println(words.contains("CCCC"));
         * System.out.println(words.contains("A"));
         */

        // Part 5
        List<Point> points = new ArrayList<Point>();
        points.add(new Point(1, 1));
        points.add(new Point(2, 2));
        points.add(new Point(3, 4));
        Point target = new Point(2, 2);

        System.out.println(points.contains(target));

    }

    public static int countUpperCase(String[] strings) {
        int counter = 0;

        for (int i = 0; i < strings.length; i++) {

            if (strings[i] != null && isUpperCase(strings[i])) {
                counter++;
            }
        }
        return counter;
    }

    public static boolean isUpperCase(String s) {
        return s.matches("^[A-Z]*$");
    }

    public static String[] getUpperCase(String[] strings) {
        int count = countUpperCase(strings);
        String[] newString = new String[count];
        int counter = 0;
        for (int i = 0; i < strings.length; i++) {
            if (strings[i] != null && isUpperCase(strings[i])) {
                newString[counter] = strings[i];
                counter++;
            }
        }
        return newString;

    }

    public static int countUpperCase(List<String> words) {
        int counter = 0;
        for (String s : words) {
            if (s != null && isUpperCase(s)) {
                counter++;
            }
        }
        return counter;
    }

    public static List<String> getUpperCase(List<String> words) {
        List<String> newArray = new ArrayList<String>();
        for (String s : words) {
            if (s != null && isUpperCase(s)) {
                newArray.add(s);
            }
        }
        return newArray;
    }

}
